<?php

/* Template Name: Main Page */

get_header(); ?>

<div class="container pb-16">

    <div data-component="title">

        <div data-component="title__icon">

            <?php echo_svg( 'icon-' . $post->ID ); ?>

        </div>

        <h1 data-component="title__text">

            <?php the_title(); ?>

        </h1>

    </div>

    <div data-grid="two" class="mb-4 lg:mb-8">

        <div data-component="box" class="bg-white">

            <div data-component="box__body">

                <p class="font-bold text-black uppercase">Description</p>

                <p><?php the_field( 'description' ); ?></p>

            </div>

        </div>

        <div>

            <a data-component="link" class="mb-4" href="#forms">

                <div data-component="link__icon">

                    <?php echo_svg( 'quill' ); ?>

                </div>

                <div data-component="link__title">

                    <p>Forms</p>

                    <?php echo_svg( 'chevron-right' ); ?>

                </div>

            </a>

            <a data-component="link" class="mb-4" href="#case-study">

                <div data-component="link__icon">

                    <?php echo_svg( 'study' ); ?>

                </div>

                <div data-component="link__title">

                    <p>Case study</p>

                    <?php echo_svg( 'chevron-right' ); ?>

                </div>

            </a>

            <a data-component="link" href="#training">

                <div data-component="link__icon">

                    <?php echo_svg( 'weights' ); ?>

                </div>

                <div data-component="link__title">

                    <p>Training</p>

                    <?php echo_svg( 'chevron-right' ); ?>

                </div>

            </a>

        </div>

    </div>

    <div data-grid="three">

        <div>

            <div data-component="box__heading" class="bg-green mb-2 rounded">

                <?php echo_svg( 'phone' ); ?>

                Telephone

            </div>

            <?php if ( have_rows( 'telephone' ) ) : ?>

                <?php while ( have_rows( 'telephone' ) ) : the_row(); ?>

                    <a href="<?php the_sub_field( 'link' ); ?>" data-component="box__link" class="bg-white" target="_blank">

                        <div>

                            <p class="font-bold mb-2 text-black"><?php the_sub_field( 'title' ); ?></p>
                            <p class="text-sm mb-0"><?php the_sub_field( 'text' ); ?></p>

                        </div>

                        <?php echo_svg( 'chevron-right' ); ?>

                    </a>

                    <?php endwhile; ?>

            <?php endif; ?>

        </div>

        <div>

            <div data-component="box__heading" class="bg-teal mb-2 rounded">

                <?php echo_svg( 'web' ); ?>

                Internet

            </div>

            <?php if ( have_rows( 'internet' ) ) : ?>

                <?php while ( have_rows( 'internet' ) ) : the_row(); ?>

                    <a href="<?php the_sub_field( 'link' ); ?>" data-component="box__link" class="bg-white" target="_blank">

                        <div>

                            <p class="font-bold mb-2 text-black"><?php the_sub_field( 'title' ); ?></p>
                            <p class="text-sm mb-0"><?php the_sub_field( 'text' ); ?></p>

                        </div>

                        <?php echo_svg( 'chevron-right' ); ?>

                    </a>

                    <?php endwhile; ?>

            <?php endif; ?>

        </div>

        <div>

            <div data-component="box__heading" class="bg-navy mb-2 rounded">

                <?php echo_svg( 'app' ); ?>

                Smartphone App

            </div>

            <?php if ( have_rows( 'smartphone_app' ) ) : ?>

                <?php while ( have_rows( 'smartphone_app' ) ) : the_row(); ?>

                    <a href="<?php the_sub_field( 'link' ); ?>" data-component="box__link" class="bg-white" target="_blank">

                        <div>

                            <p class="font-bold mb-2 text-black"><?php the_sub_field( 'title' ); ?></p>
                            <p class="text-sm mb-0"><?php the_sub_field( 'text' ); ?></p>

                        </div>

                        <?php echo_svg( 'chevron-right' ); ?>

                    </a>

                    <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>

</div>

<div class="bg-grey-light py-16">

    <div data-grid="sidebar" class="container">

        <div data-component="box" class="bg-teal">

            <div data-component="box__heading" class="border-b border-fade text-white" id="forms">

                <?php echo_svg( 'quill' ); ?>

                Forms

            </div>

            <div data-component="box__body" class="text-white">

                <?php the_field( 'forms_text' ); ?>

                <a data-component="button" class="bg-green hover:bg-green-dark" href="<?php the_field( 'forms_link' ); ?>" target="_blank">
                    Download forms here
                </a>

            </div>

        </div>

        <div data-component="box" class="bg-white">

            <div data-component="box__heading" class="border-b svg-blue text-black" id="case-study">

                <?php echo_svg( 'study' ); ?>

                Case Study

            </div>

            <div data-component="box__body">

                <?php the_field( 'case_study' ); ?>

            </div>

        </div>

    </div>

</div>

<div class="bg-grey-lighter bg-pattern py-16">

    <div data-grid="three" class="container">

        <div data-component="box" class="bg-white">

            <div data-component="box__heading" class="border-b svg-blue text-black" id="training">

                <?php echo_svg( 'weights' ); ?>

                Training resources

            </div>

            <div data-component="box__body">

                <?php the_field( 'training' ); ?>

            </div>

        </div>

        <div data-component="box" class="bg-white">

            <div data-component="box__heading" class="border-b svg-blue text-black">

                <?php echo_svg( 'play' ); ?>

                Videos

            </div>

            <div data-component="box__body">

                <?php the_field( 'video' ); ?>

            </div>

        </div>

        <div>

            <?php if ( have_rows( 'links' ) ) : ?>

                <?php while ( have_rows( 'links' ) ) : the_row(); ?>

                    <a data-component="link" class="mb-4" href="<?php the_sub_field( 'url' ); ?>" target="_blank">

                        <div data-component="link__title" class="rounded-l">

                            <p><?php the_sub_field( 'text' ); ?></p>

                            <?php echo_svg( 'chevron-right' ); ?>

                        </div>

                    </a>

                    <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
