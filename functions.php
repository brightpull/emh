<?php

get_template_part( 'includes/cleanup' );
get_template_part( 'includes/cpt' );
get_template_part( 'includes/customisations' );
get_template_part( 'includes/helpers' );

// Add theme support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// Enqueue scripts
function bright_load_assets()
{
    // jQuery
    if ( !is_admin() ) { wp_deregister_script( 'jquery' ); }
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', true );

    // CSS
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css' );

    // JavaScript
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@next/dist/aos.js', array(), '3.0.0', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'bright_load_assets' );

// Button shortcode
function bright_button_shortcode( $atts ) {

    extract(shortcode_atts(array(
        'link' => 'https://emhtoolkit.com.au',
        'text' => 'Button'
    ), $atts));

    return '<a data-component="button" class="bg-blue hover:bg-blue-dark" href="' . $link . '">' . $text . '</a>';
}
add_shortcode('button', 'bright_button_shortcode');
