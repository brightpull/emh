<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <meta name="description" content="Simple bedrock for WordPress themes.">
    <meta name="author" content="Tim Martin">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div data-component="menu" class="bg-blue text-sm text-white">

    <div class="border-b border-fade pb-4">

        <ul class="font-bold uppercase">
            <li>
                <a class="svg-4 svg-fill text-right" href="#" data-js="toggle-menu">
                    <?php echo_svg( 'chevron-left' ); ?>
                </a>
            </li>
            <li><a href="<?php echo site_url(); ?>">Home</a></li>
        </ul>

    </div>

    <div class="py-4">

        <ul class="font-bold uppercase">
            <li><a href="<?php echo get_permalink( 13 ); ?>"><?php echo get_the_title( 13 ); ?></a></li>
            <li><a href="<?php echo get_permalink( 15 ); ?>"><?php echo get_the_title( 15 ); ?></a></li>
            <li><a href="<?php echo get_permalink( 9 ); ?>"><?php echo get_the_title( 9 ); ?></a></li>
            <li><a href="<?php echo get_permalink( 17 ); ?>"><?php echo get_the_title( 17 ); ?></a></li>
        </ul>

    </div>

    <div class="border-fade border-t py-4">

        <ul>
            <li><a href="<?php echo get_permalink( 11 ); ?>"><?php echo get_the_title( 11 ); ?></a></li>
        </ul>

    </div>

    <div class="border-fade border-t py-4">

        <ul>
            <li><a href="https://www.swsphn.com.au/feedback" target="_blank">Provide Feedback</a></li>
            <li><a href="https://www.swsphn.com.au/" target="_blank">SWSPHN Website</a></li>
        </ul>

    </div>

</div>

<header class="py-8">

    <div class="container svg-6 svg-fill">

        <a class="text-navy" href="#" data-js="toggle-menu">
            <?php echo_svg( 'bars' ); ?>
        </a>

    </div>

</header>
