<?php

/* Template Name: Services */

get_header(); ?>

<div class="container pb-16" id="specialty">

    <div data-component="title" class="mb-12">

        <div data-component="title__icon">

            <?php echo_svg( 'pulse' ); ?>

        </div>

        <h1 data-component="title__text">

            <?php the_title(); ?>

        </h1>

    </div>

    <h2 class="text-base text-black uppercase">Specialty Services</h2>

    <div data-grid="four">

        <?php query_posts( 'post_type=service&meta_key=group&meta_value=Specialty' ); ?>

        <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php echo get_template_part( 'parts/service' ); ?>

            <?php endwhile; ?>

        <?php endif; ?>

        <?php wp_reset_query(); ?>

    </div>

</div>

<div class="bg-grey-light py-16" id="national">

    <div class="container">

        <h2 class="text-base text-black uppercase">National Information Portals</h2>

        <div data-grid="four">

            <?php query_posts( 'post_type=service&meta_key=group&meta_value=National' ); ?>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php echo get_template_part( 'parts/service' ); ?>

                <?php endwhile; ?>

            <?php endif; ?>

            <?php wp_reset_query(); ?>

        </div>

    </div>

</div>

<div class="bg-grey-lighter py-16" id="other">

    <div class="container">

        <h2 class="text-base text-black uppercase">Other Support Services</h2>

        <div data-grid="four">

            <?php query_posts( 'post_type=service&meta_key=group&meta_value=Other' ); ?>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php echo get_template_part( 'parts/service' ); ?>

                <?php endwhile; ?>

            <?php endif; ?>

            <?php wp_reset_query(); ?>

        </div>

    </div>

</div>

<div class="bg-grey-light bg-pattern py-16" id="regional">

    <div class="container">

        <h2 class="text-base text-black uppercase">Regional (SWSPHN) Services</h2>

        <div data-grid="four">

            <?php query_posts( 'post_type=service&meta_key=group&meta_value=Regional' ); ?>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php echo get_template_part( 'parts/service' ); ?>

                <?php endwhile; ?>

            <?php endif; ?>

            <?php wp_reset_query(); ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>
