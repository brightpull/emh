// Animate on scroll
AOS.init();

// Menu
$('[data-js="toggle-menu"]').click(function(event) {
    event.preventDefault();
    $('[data-component="menu"]').toggleClass('open');
});
