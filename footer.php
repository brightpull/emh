<footer class="bg-navy py-12">

    <div class="container text-sm text-white md:flex md:items-center md:justify-end md:text-right">

        <p class="md:mb-0"><strong class="text-green">Phone:</strong> 02 4632 3000<br>
        <strong class="text-green">Email:</strong> enquiries@swsphn.com.au</p>

        <p class="md:mb-0 md:pl-12">Level 3, 1 Bolger Street Campbelltown NSW 2560<br>
        PO Box 90, Macarthur Square NSW 2560</p>

        <p class="mb-0 md:pl-16"><img src="<?php echo get_template_directory_uri(); ?>/images/phn-logo.png" alt="PHN logo" width="100"></p>

    </div>

</footer>

<?php wp_footer(); ?>

</body>

</html>
