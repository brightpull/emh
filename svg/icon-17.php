<svg xmlns="http://www.w3.org/2000/svg" width="47.404" height="65.771" viewBox="0 0 47.404 65.771">
  <g id="Group_94" data-name="Group 94" transform="translate(-1290.473 -753.305)">
    <line id="Line_34" data-name="Line 34" x2="28.269" transform="translate(1292.973 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <line id="Line_35" data-name="Line 35" x2="37.692" transform="translate(1307.108 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <path id="Path_117" data-name="Path 117" d="M0,0H50.256" transform="translate(1321.242 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <path id="Path_118" data-name="Path 118" d="M0,0H60.771" transform="translate(1335.377 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5" opacity="0.228"/>
  </g>
</svg>
