<svg xmlns="http://www.w3.org/2000/svg" width="47.979" height="65.771" viewBox="0 0 47.979 65.771">
  <g id="Group_95" data-name="Group 95" transform="translate(-1664 -753.305)">
    <line id="Line_38" data-name="Line 38" x2="28.269" transform="translate(1666.5 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <line id="Line_39" data-name="Line 39" x2="37.692" transform="translate(1681.21 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <path id="Path_121" data-name="Path 121" d="M0,0H50.256" transform="translate(1695.344 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <path id="Path_122" data-name="Path 122" d="M0,0H60.771" transform="translate(1709.479 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
  </g>
</svg>
