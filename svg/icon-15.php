<svg xmlns="http://www.w3.org/2000/svg" width="47.404" height="65.771" viewBox="0 0 47.404 65.771">
  <g id="Group_93" data-name="Group 93" transform="translate(-926.666 -753.305)">
    <line id="Line_36" data-name="Line 36" x2="28.269" transform="translate(929.166 816.576) rotate(-90)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
    <line id="Line_37" data-name="Line 37" x2="37.692" transform="translate(943.301 816.576) rotate(-90)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5"/>
    <path id="Path_119" data-name="Path 119" d="M0,0H50.256" transform="translate(957.435 816.576) rotate(-90)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5" opacity="0.23"/>
    <path id="Path_120" data-name="Path 120" d="M0,0H60.771" transform="translate(971.57 816.576) rotate(-90)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="5" opacity="0.228"/>
  </g>
</svg>
