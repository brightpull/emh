<svg xmlns="http://www.w3.org/2000/svg" width="47.404" height="65.771" viewBox="0 0 47.404 65.771">
  <g id="Group_92" data-name="Group 92" transform="translate(-550 -753.305)">
    <line id="Line_32" data-name="Line 32" x2="28.269" transform="translate(552.5 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5"/>
    <line id="Line_33" data-name="Line 33" x2="37.692" transform="translate(566.635 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5" opacity="0.23"/>
    <path id="Path_115" data-name="Path 115" d="M0,0H50.256" transform="translate(580.769 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5" opacity="0.23"/>
    <path id="Path_116" data-name="Path 116" d="M0,0H60.771" transform="translate(594.904 816.576) rotate(-90)" fill="none" stroke="#eaeaea" stroke-linecap="round" stroke-width="5" opacity="0.23"/>
  </g>
</svg>
