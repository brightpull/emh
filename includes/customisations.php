<?php

// Hide admin bar
show_admin_bar(false);

// Dashboard thank you message
function bright_change_thank_you_message()
{
    echo '<span id="footer-thankyou">Made with <3 by <a href="https://brightagency.com.au" target="_blank">Bright</a></span>.';
}
add_filter( 'admin_footer_text', 'bright_change_thank_you_message' );

// Hide dashboard menu items
function bright_remove_admin_pages()
{
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'upload.php' );
    remove_menu_page( 'edit-comments.php' );
    // remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'bright_remove_admin_pages' );

// Hide editor
function bright_hide_editor()
{
    remove_post_type_support( 'page', 'editor' );
}
add_action( 'admin_init', 'bright_hide_editor' );
