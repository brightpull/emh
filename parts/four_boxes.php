<div data-grid="four">

    <div data-component="box" class="bg-white" data-aos="fade-up" data-aos-delay="250">

        <div data-component="box__heading" class="bg-navy">

            <?php echo_svg( 'icon-13' ); ?>

        </div>

        <div data-component="box__body">

            <p class="font-bold text-black uppercase"><?php echo get_the_title( 13 ); ?></p>

            <p><?php the_field( 'intro', 13 ); ?></p>

            <a data-component="button" class="bg-navy hover:bg-navy-dark w-full" href="<?php echo get_permalink( 13 ); ?>">
                View Resources
            </a>

        </div>

    </div>

    <div data-component="box" class="bg-white" data-aos="fade-up" data-aos-delay="500">

        <div data-component="box__heading" class="bg-teal">

            <?php echo_svg( 'icon-15' ); ?>

        </div>

        <div data-component="box__body">

            <p class="font-bold text-black uppercase"><?php echo get_the_title( 15 ); ?></p>

            <p><?php the_field( 'intro', 15 ); ?></p>

            <a data-component="button" class="bg-teal hover:bg-teal-dark w-full" href="<?php echo get_permalink( 15 ); ?>">
                View Resources
            </a>

        </div>

    </div>

    <div data-component="box" class="bg-white" data-aos="fade-up" data-aos-delay="750">

        <div data-component="box__heading" class="bg-blue">

            <?php echo_svg( 'icon-17' ); ?>

        </div>

        <div data-component="box__body">

            <p class="font-bold text-black uppercase"><?php echo get_the_title( 9 ); ?></p>

            <p><?php the_field( 'intro', 9 ); ?></p>

            <a data-component="button" class="bg-blue hover:bg-blue-dark w-full" href="<?php echo get_permalink( 9 ); ?>">
                View Resources
            </a>

        </div>

    </div>

    <div data-component="box" class="bg-white" data-aos="fade-up" data-aos-delay="1000">

        <div data-component="box__heading" class="bg-green">

            <?php echo_svg( 'icon-9' ); ?>

        </div>

        <div data-component="box__body">

            <p class="font-bold text-black uppercase"><?php echo get_the_title( 17 ); ?></p>

            <p><?php the_field( 'intro', 17 ); ?></p>

            <a data-component="button" class="bg-green hover:bg-green-dark w-full" href="<?php echo get_permalink( 17 ); ?>">
                View Resources
            </a>

        </div>

    </div>

</div>
