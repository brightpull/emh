<a href="<?php the_field('url'); ?>" data-component="service" target="_blank">

    <?php if ( get_field( 'category' ) == 'Children' ) : ?>

        <div data-component="service__category" class="bg-green">
            <?php the_field( 'category' ); ?>
        </div>

    <?php elseif ( get_field( 'category' ) == 'Teenagers' ) : ?>

        <div data-component="service__category" class="bg-navy">
            <?php the_field( 'category' ); ?>
        </div>

    <?php endif; ?>

    <div class="p-8 text-center">

        <?php if ( get_field( 'logo' ) ) : ?>
            <img class="mb-6" src="<?php the_field( 'logo' ); ?>" alt="logo" style="max-height: 80px">
        <?php endif; ?>

        <h4 class="mb-2 text-base text-black uppercase"><?php the_title(); ?></h4>

        <p class="mb-0"><?php the_field( 'label' ); ?></p>

    </div>

</a>
