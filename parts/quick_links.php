<div data-grid="two">

    <div>

        <a data-component="link" href="<?php echo get_permalink( 11 ); ?>#specialty">

            <div data-component="link__icon">

                <?php echo_svg( 'search' ); ?>

            </div>

            <div data-component="link__title">

                <p>Specialty services</p>

                <?php echo_svg( 'chevron-right' ); ?>

            </div>

        </a>

    </div>

    <div>

        <a data-component="link" href="<?php echo get_permalink( 11 ); ?>#national">

            <div data-component="link__icon">

                <?php echo_svg( 'tv' ); ?>

            </div>

            <div data-component="link__title">

                <p>National information portals</p>

                <?php echo_svg( 'chevron-right' ); ?>

            </div>

        </a>

    </div>

    <div>

        <a data-component="link" href="<?php echo get_permalink( 11 ); ?>#other">

            <div data-component="link__icon">

                <?php echo_svg( 'plus' ); ?>

            </div>

            <div data-component="link__title">

                <p>Other support services</p>

                <?php echo_svg( 'chevron-right' ); ?>

            </div>

        </a>

    </div>

    <div>

        <a data-component="link" href="<?php echo get_permalink( 11 ); ?>#regional">

            <div data-component="link__icon">

                <?php echo_svg( 'pin' ); ?>

            </div>

            <div data-component="link__title">

                <p>Regional (SWSPHN) services</p>

                <?php echo_svg( 'chevron-right' ); ?>

            </div>

        </a>

    </div>

</div>
