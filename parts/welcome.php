<div data-grid="two" class="bg-welcome mb-4 p-8 rounded shadow text-white lg:px-12 xl:px-16 xl:py-16">

    <div>

        <div class="md:pr-4" data-aos="fade" data-aos-delay="500">

            <p class="font-bold text-xl">Welcome to</p>

            <h1 class="text-monster">The eMental Health Toolkit</h1>

        </div>

    </div>

    <div>

        <div class="md:pl-4" data-aos="fade-left" data-aos-delay="750">

            <p class="font-bold uppercase">About the Toolkit</p>

            <p><?php the_field( 'about', 2 ); ?></p>

            <a data-component="button" class="bg-blue hover:bg-blue-dark mt-2" href="https://www.youtube.com/watch?v=06B6D6Dz8GE" target="_blank">
                <div class="flex items-center justify-between">
                    <span>What is Primary Healthcare?</span>
                    <?php echo_svg( 'play' ); ?>
                </div>
            </a>

        </div>

    </div>

</div>
