<?php get_header(); ?>

<div class="bg-happy">

    <div class="container pb-12">

        <?php echo get_template_part( 'parts/welcome' ); ?>

        <?php echo get_template_part( 'parts/four_boxes' ); ?>

        <div class="pb-10 pt-16 text-center md:px-16 lg:px-32">

            <p class="lg:px-24"><?php the_field( 'information', 2 ); ?></p>

        </div>

        <?php echo get_template_part( 'parts/quick_links' ); ?>

    </div>

</div>

<?php get_footer(); ?>
